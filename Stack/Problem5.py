def findingSpan(array):
    output = [None] * len(array)
    output[0] = 1
    for i in range(1, len(array)):
        j = i
        count = 1
        while j >= 0 and array[i] >= array[j - 1]:
            count = count + 1
            j = j - 1
        output[i] = count
    print(output)

findingSpan([10, 4, 5, 90, 120, 80])
