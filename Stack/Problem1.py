class Stack:

    def __init__(self):
        self.stackItems = []
        
    def push(self, data):
        self.stackItems.append(data)
    
    def pop(self):
        return self.stackItems.pop()
    
    def checkBalanedParanthesis(self, inputString):
        isParantheisBalanced = False
        for item in inputString:
            if item in ["(", "[", "{"]:
                self.stackItems.append(item)
            else:
                if len(self.stackItems) <= 0:
                    isParantheisBalanced = False
                else:
                    popedItem = self.pop()
                    if (popedItem == "(" and item == ")") or (popedItem == "{" and item == "}") or (popedItem == "[" and item == "]"):
                        isParantheisBalanced = True
                    else:
                        isParantheisBalanced = False
            if len(self.stackItems) > 0:
                isParantheisBalanced = False
        return isParantheisBalanced

stack = Stack()
print(stack.checkBalanedParanthesis("[)]")) # False
print(stack.checkBalanedParanthesis("{{([][])}()}")) # True
print(stack.checkBalanedParanthesis("{[]{()}}")) # True
print(stack.checkBalanedParanthesis("[{}{})(]")) # False
print(stack.checkBalanedParanthesis("((()")) #False