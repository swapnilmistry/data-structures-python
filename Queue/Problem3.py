# Example :
# Input: arr[] = {1, 2, 3, 1, 4, 5, 2, 3, 6}, K = 3
# Output: 3 3 4 5 5 5 6
# Explanation:
# Maximum of 1, 2, 3 is 3
# Maximum of 2, 3, 1 is 3
# Maximum of 3, 1, 4 is 4
# Maximum of 1, 4, 5 is 5
# Maximum of 4, 5, 2 is 5
# Maximum of 5, 2, 3 is 5
# Maximum of 2, 3, 6 is 6

# Input: arr[] = {8, 5, 10, 7, 9, 4, 15, 12, 90, 13}, K = 4
# Output: 10 10 10 15 15 90 90
# Explanation:
# Maximum of first 4 elements is 10, similarly for next 4
# elements (i.e from index 1 to 4) is 10, So the sequence
# generated is 10 10 10 15 15 90 90


def slidingWindowMaximum(window, k):
    windowLength = len(window)
    for i in range(0, windowLength):
        if window[i:i+k] and len(window[i:i+k]) == k:
            print(max(window[i:i+k]), end=" ")
        else:
            return

window = [1, 2, 3, 1, 4, 5, 2, 3, 6]
slidingWindowMaximum(window, 3)
print("\n")
window = [8, 5, 10, 7, 9, 4, 15, 12, 90, 13]
slidingWindowMaximum(window, 4)
print("\n")
window = [1, 3, -1, -3, 5, 3, 6, 7]
slidingWindowMaximum(window, 3)

# Python program to find the maximum for 
# each and every contiguous subarray of 
# size k 

# from collections import deque 

# # A Deque (Double ended queue) based 
# # method for printing maximum element 
# # of all subarrays of size k 
# def printMax(arr, n, k): 
	
# 	""" Create a Double Ended Queue, Qi that 
# 	will store indexes of array elements. 
# 	The queue will store indexes of useful 
# 	elements in every window and it will 
# 	maintain decreasing order of values from 
# 	front to rear in Qi, i.e., arr[Qi.front[]] 
# 	to arr[Qi.rear()] are sorted in decreasing 
# 	order"""
# 	Qi = deque() 
	
# 	# Process first k (or first window) 
# 	# elements of array 
# 	for i in range(k): 
		
# 		# For every element, the previous 
# 		# smaller elements are useless 
# 		# so remove them from Qi 
# 		while Qi and arr[i] >= arr[Qi[-1]] : 
# 			Qi.pop() 
		
# 		# Add new element at rear of queue 
# 		Qi.append(i); 
		
# 	# Process rest of the elements, i.e. 
# 	# from arr[k] to arr[n-1] 
# 	for i in range(k, n): 
		
# 		# The element at the front of the 
# 		# queue is the largest element of 
# 		# previous window, so print it 
# 		print(str(arr[Qi[0]]) + " ", end = "") 
		
# 		# Remove the elements which are 
# 		# out of this window 
# 		while Qi and Qi[0] <= i-k: 
			
# 			# remove from front of deque 
# 			Qi.popleft() 
		
# 		# Remove all elements smaller than 
# 		# the currently being added element 
# 		# (Remove useless elements) 
# 		while Qi and arr[i] >= arr[Qi[-1]] : 
# 			Qi.pop() 
		
# 		# Add current element at the rear of Qi 
# 		Qi.append(i) 
	
# 	# Print the maximum element of last window 
# 	print(str(arr[Qi[0]])) 
	
# # Driver programm to test above fumctions 
# if __name__=="__main__": 
# 	arr = [12, 1, 78, 90, 57, 89, 56] 
# 	k = 3
# 	printMax(arr, len(arr), k) 
