class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class Queue:
    def __init__(self):
        self.front = None
        self.rear = None
    
    def enqueue(self, data):
        newQeueueElement = Node(data)
        if self.rear == None:
            self.front = self.rear = newQeueueElement
            return
        self.rear.next = newQeueueElement
        self.rear = newQeueueElement
        self.rear.next = None
    
    def dequeue(self):
        if self.front == None:
            print("Queue Underflow")
            return
        temp = self.front
        self.front = temp.next
        if self.front == None:
            self.rear = None
    
    def printQueueElements(self):
        current = self.front
        while current != None:
            print(current.data, end = " ")
            current = current.next


queue = Queue()
queue.enqueue(1)
queue.enqueue(2)
queue.enqueue(3)
queue.enqueue(4)
queue.enqueue(5)
queue.printQueueElements()
queue.dequeue()
queue.printQueueElements()