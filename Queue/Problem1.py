class Queue:
    def __init__(self):
        self.queue = []

    def enqueue(self, data):
        self.queue.append(data)

    def deqeue(self):
        self.queue.pop(0)

    def printQueueElements(self):
        print(self.queue)


queue = Queue()
queue.enqueue(1)
queue.enqueue(2)
queue.enqueue(3)
queue.enqueue(4)
queue.enqueue(5)
queue.printQueueElements()
queue.deqeue()
queue.printQueueElements()
