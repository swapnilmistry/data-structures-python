# Topological sorting for Directed Acyclic Graph (DAG) is a linear ordering of vertices such that for every directed edge u v, ]
# vertex u comes before v in the ordering. 
# Topological Sorting for a graph is not possible if the graph is not a DAG.

class Graph:
    def __init__(self):
        self.visited = set()
        self.stack = []
        self.graph = {
            "5": ["2", "0"],
            "4": ["0", "1"],
            "2": ["3"],
            "0": [],
            "3": ["1"],
            "1": [],
        }
    
    def topologicalSortUtil(self, vertex):
        self.visited.add(vertex)
        for neigbhour in self.graph[vertex]:
            if neigbhour not in self.visited:
                self.topologicalSortUtil(neigbhour)
        self.stack.append(vertex)

    def topologicalSort(self):
        for vertex in self.graph:
            if vertex not in self.visited:
                self.topologicalSortUtil(vertex)
        print(self.stack[::-1])

graph = Graph()
graph.topologicalSort()
