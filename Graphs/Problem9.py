from collections import defaultdict
class Graph:

    def __init__(self, numberOfVertices):
        self.vertices = numberOfVertices
        self.graph = defaultdict(list)
        self.time = 0
    
    def addEdge(self, u, v):
        self.graph[u].append(v)
    
    #The function to do DFS traversal.
    # It uses recursive SCCUtil()
    def scc(self):
        # Mark all the vertices as not visited
        # and Initialize parent and visited,
        # and ap(articulation point) arrays
        visited = [False] * self.vertices
        low = [-1] * self.vertices
        disc = [-1] * self.vertices
        stack = []
        # Call the recursive helper function
        # to find articulation points
        # in DFS tree rooted with vertex 'i'
        for i in range(self.vertices):
            if disc[i] == -1:
                self.sccUtil(visited, low, disc, i, stack)
    
    '''A recursive function that find finds and prints strongly connected
    components using DFS traversal
    u --> The vertex to be visited next
    disc[] --> Stores discovery times of visited vertices
    low[] -- >> earliest visited vertex (the vertex with minimum
                discovery time) that can be reached from subtree
                rooted with current vertex
     stack -- >> To store all the connected ancestors (could be part
           of SCC)
     visited[] --> bit/index array for faster check whether
                  a node is in stack
    '''
    def sccUtil(self, visited, low, disc, u, stack):
        disc[u] = self.time
        low[u] = self.time
        self.time = self.time + 1
        visited[u] = True
        stack.append(u)
        for v in self.graph[u]:
            if disc[v] == -1:
                self.sccUtil(visited, low, disc, v, stack)
                low[u] = min(low[u], low[v])
            elif visited[v] == True:
                low[u] = min(low[u], disc[v])
        w = -1
        if low[u] == disc[u]:
            while w != u:
                w = stack.pop()
                print(w, end=" ")
                visited[w] = False
            print("\n")

g1 = Graph(5)
g1.addEdge(1, 0)
g1.addEdge(0, 2)
g1.addEdge(2, 1)
g1.addEdge(0, 3)
g1.addEdge(3, 4)
print("nSSC in first graph:")
g1.scc()
 
g2 = Graph(4)
g2.addEdge(0, 1)
g2.addEdge(1, 2)
g2.addEdge(2, 3)
print("nSSC in second graph:")
g2.scc()
 
  
g3 = Graph(7)
g3.addEdge(0, 1)
g3.addEdge(1, 2)
g3.addEdge(2, 0)
g3.addEdge(1, 3)
g3.addEdge(1, 4)
g3.addEdge(1, 6)
g3.addEdge(3, 5)
g3.addEdge(4, 5)
print("nSSC in third graph:")
g3.scc()
 
g4 = Graph(11)
g4.addEdge(0, 1)
g4.addEdge(0, 3)
g4.addEdge(1, 2)
g4.addEdge(1, 4)
g4.addEdge(2, 0)
g4.addEdge(2, 6)
g4.addEdge(3, 2)
g4.addEdge(4, 5)
g4.addEdge(4, 6)
g4.addEdge(5, 6)
g4.addEdge(5, 7)
g4.addEdge(5, 8)
g4.addEdge(5, 9)
g4.addEdge(6, 4)
g4.addEdge(7, 9)
g4.addEdge(8, 9)
g4.addEdge(9, 8)
print("nSSC in fourth graph:")
g4.scc();
 
 
g5 = Graph (5)
g5.addEdge(0, 1)
g5.addEdge(1, 2)
g5.addEdge(2, 3)
g5.addEdge(2, 4)
g5.addEdge(3, 0)
g5.addEdge(4, 2)
print("nSSC in fifth graph:")
g5.scc();