class Graph:
    def __init__(self):
        self.visited = set()
        self.graph = {
            "A": ["B", "C"],
            "B": ["D", "E"],
            "C": ["F"],
            "D": [],
            "E": ["F"],
            "F": []
        }

    def DFS(self, startVertex):
        if startVertex not in self.visited:
            self.visited.add(startVertex)
            print(startVertex, end = " ")
        for v in self.graph[startVertex]:
            if v not in self.visited:
                self.DFS(v)

graph = Graph()
graph.DFS("A")

# class Graph:
#     def __init__(self):
#         self.stack = []
#         self.visited = []
#         self.graph = {
#             "A": ["B", "C"],
#             "B": ["D", "E"],
#             "C": ["F"],
#             "D": [],
#             "E": ["F"],
#             "F": []
#         }

#     def DFS(self, startVertex):
#         if startVertex not in self.stack:
#             self.stack.append(startVertex)
#             self.visited.append(startVertex)
#             print(startVertex, end = " ")
#             print(self.stack)
#         for v in self.graph[startVertex]:
#             if v not in self.visited:
#                 self.DFS(v)

# graph = Graph()
# graph.DFS("A")


# Input 1

# {
#     "0": ["1", "2", "4"],
#     "1": [],
#     "2": [],
#     "3": [],
#     "4": ["3"],
# }

# Input 2

# {
#     "0": ["1", "3"],
#     "1": ["2"],
#     "2": [],
#     "3": [],
# }
