import heapq
class Graph:
    def __init__(self):
        self.graph = {
            "A": [["B", 4], ["C", 2]],
            "B": [["C", 5], ["D", 10]],
            "C": [["E", 3]],
            "D": [["F", 11]],
            "E": [["D", 4]],
            "F": []
        }
    
    def dijkstras(self, source, destination):
        heap = []
        heapq.heappush(heap, (0, source))
        while len(heap) != 0:
            cost, vertex = heapq.heappop(heap)
            if vertex == destination:
                print("Path exists {} to {} with cost {}".format(source, destination, cost));
                break
            for neighbour in self.graph[vertex]:
                heapq.heappush(heap, (cost+neighbour[1], neighbour[0]))

g = Graph()
g.dijkstras("A", "D")
