'''
                A
               / \
              B   C
             /\    \
            D  E -  F

'''
from queue import Queue

class Graph:
    def __init__(self):
        self.queue = Queue()
        self.visited = []
        self.graph = {
            "A": ["B", "C"],
            "B": ["D", "E"],
            "C": ["F"],
            "D": [],
            "E": ["F"],
            "F": []
        }

    def BFS(self, startVertex):
        self.queue.put(startVertex)
        self.visited.append(startVertex)
        while not self.queue.empty():
            s = self.queue.get()
            print(s, end = " ")
            for v in self.graph[s]:
                if v not in self.visited:
                    self.queue.put(v)
                    self.visited.append(v)


graph = Graph()
graph.BFS("A")