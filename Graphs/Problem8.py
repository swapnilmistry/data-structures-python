class Graph:
    def __init__(self):
        self.visited = set()
        self.graph = {
            "0": ["1", "2", "3"],
            "1": ["3"],
            "2": ["0", "1"],
            "3": []
        }
        self.visited = set()

    def printAllPaths(self, source, destination, path):
        if source == destination:
            print(path)
        else:
            if source not in self.visited:
                self.visited.add(source)
            for v in self.graph[source]:
                if v not in self.visited:
                    self.printAllPaths(v, destination, path+"=>"+v)
        if source in self.visited:
            self.visited.remove(source)
        

g = Graph()
g.printAllPaths("2", "3", "2")



