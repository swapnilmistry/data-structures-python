class Graph:
    def __init__(self):
        self.graph = [];
        self.numberOfVerticesInGraph = 0;
        self.vertices = [];
    
    # def createVetices(self, numberOfVertices):
    #     self.graph = [[0]*numberOfVertices for _ in range(0, numberOfVertices)]

    def addVertex(self, vertex):
        self.numberOfVerticesInGraph = self.numberOfVerticesInGraph + 1
        self.vertices.append(vertex)
        if self.numberOfVerticesInGraph > 1:
           for vertex in self.graph:
               vertex.append(0)
        temp = []
        for _ in range(self.numberOfVerticesInGraph):
            temp.append(0)
        self.graph.append(temp)
    
    def addEdge(self, vertex1, vertex2, weight):
        if vertex1 not in self.vertices:
            print("Vertex {vertex1} not present in graph")
        elif vertex2 not in self.vertices:
            print("Vertex {vertex2} not present in graph")
        else:
            row = self.vertices.index(vertex1)
            column = self.vertices.index(vertex2)
            self.graph[row][column] = weight
    
    def printGraph(self):
        for i in range(self.numberOfVerticesInGraph):
            for j in range(self.numberOfVerticesInGraph):
                if self.graph[i][j] != 0:
                    print(self.vertices[i],  " =>", self.vertices[j], "Edge Weight:", self.graph[i][j])
                
    

graph = Graph()
# graph.createVetices(4)
graph.addVertex(1)
graph.addVertex(2)
graph.addVertex(3)
graph.addVertex(4)
graph.addEdge(1, 2, 1)
graph.addEdge(1, 3, 1)
graph.addEdge(2, 3, 3)
graph.addEdge(3, 4, 4)
graph.addEdge(4, 1, 5)
graph.printGraph()

# graph.addVertex("A")
# graph.addVertex("B")
# graph.addVertex("C")
# graph.addVertex("D")
# graph.addEdge("A", "B", 1)
# graph.addEdge("A", "C", 1)
# graph.addEdge("B", "C", 3)
# graph.addEdge("C", "D", 4)
# graph.addEdge("D", "A", 5)