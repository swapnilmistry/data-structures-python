class Graph:
    def __init__(self):
        self.graph = {}
        self.numberOfVertices = 0
    
    def addVertex(self, vertex):
        self.numberOfVertices = self.numberOfVertices + 1
        if vertex in self.graph:
            print("Vertex already exists")
            return
        self.graph[vertex] = []
    
    def addEdges(self, vertex1, vertex2, weight):
        if vertex1 not in self.graph:
            print("Vertex {vertex1} dose not exists in graph")
        elif vertex2 not in self.graph:
            print("Vertex {vertex2} dose not exists in graph")
        else:
            self.graph[vertex1].append([vertex2, weight])

    
    def printGraph(self):
        for vertex in self.graph:
            for edge in self.graph[vertex]:
                print(vertex, "=>", edge[0], "Edge Weight:", edge[1])

graph = Graph()
graph.addVertex(1)
graph.addVertex(2)
graph.addVertex(3)
graph.addVertex(4)
graph.addEdges(1, 2, 1)
graph.addEdges(1, 3, 1)
graph.addEdges(2, 3, 3)
graph.addEdges(3, 4, 4)
graph.addEdges(4, 1, 5)
graph.printGraph()
print("Internal representation: ", graph.graph)