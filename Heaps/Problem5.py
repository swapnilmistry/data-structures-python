def mergeTwoMaxHeaps(heap1, heap2):
    if heap1 is None and heap2 is None:
        return None
    if heap1 is None:
        print(heap1)
        return
    if heap2 is None:
        print(heap2)
        return
    heap1 = heap1 + heap2
    for i in range ((len(heap1) // 2) - 1, -1, -1):
        heapify(heap1, len(heap1), i)
    print(heap1)

def heapify(heap, lengthOfHeap, i):
    leftChildIndex = 2*i + 1
    rigthChildIndex = 2*i + 2
    if leftChildIndex < lengthOfHeap and heap[leftChildIndex] > heap[i]:
        heap[leftChildIndex], heap[i] = heap[i], heap[leftChildIndex]
        heapify(heap, lengthOfHeap, leftChildIndex)
    if rigthChildIndex < lengthOfHeap and heap[rigthChildIndex] > heap[i]:
        heap[rigthChildIndex], heap[i] = heap[i], heap[rigthChildIndex]
        heapify(heap, lengthOfHeap, rigthChildIndex)


heap1 = [10, 5, 6, 2]
heap2 = [12, 7, 9]
mergeTwoMaxHeaps(heap1, heap2)