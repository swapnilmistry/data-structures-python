def findKSmallestElement(heap, k):
    if k > len(heap):
        print("Cannot find element.")
        return
    for i in range (0, k-1):
        heap[0], heap[len(heap) - 1] = heap[len(heap) - 1], heap[0]
        heap = heap[:len(heap)-1]
        firstNonLeafNode = ((len(heap)) // 2) - 1
        for i in range(firstNonLeafNode, -1, -1):
            heapify(heap, len(heap), i)
    print(heap[0])

def heapify(heap, n, i):
    leftChildIndex = 2*i + 1
    rightChildIndex = 2*i + 2
    if leftChildIndex < n and heap[leftChildIndex] < heap[i]:
        heap[leftChildIndex], heap[i] = heap[i], heap[leftChildIndex]
        heapify(heap, n, leftChildIndex)
    if rightChildIndex < n and heap[rightChildIndex] < heap[i]:
        heap[rightChildIndex], heap[i] = heap[i], heap[rightChildIndex]
        heapify(heap, n, rightChildIndex)

# [10, 25, 23, 45, 30, 50]
# [1, 9, 22, 17, 11, 33, 27, 21, 19]
minHeap = [10, 50, 40, 75, 60, 65, 45]
findKSmallestElement(minHeap, 2)