def getLessKElements(heap, k):
    size = len(heap)
    for i in range(0, size):
        if heap[i] < k:
            print(heap[i], end=" ")

# [10, 25, 23, 45, 30, 50]
heap = [1, 9, 22, 17, 11, 33, 27, 21, 19]
k = 30
getLessKElements(heap, k)