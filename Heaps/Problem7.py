import heapq

class FindMedian:
    def __init__(self):
        self.minHeap = []
        self.maxHeap = []

    def insertItem(self, value):
        if not len(self.maxHeap) or self.maxHeap[0] > value:
            self.maxHeap.append(value)
            heapq._heapify_max(self.maxHeap)
        else:
            self.minHeap.append(value)
            heapq.heapify(self.minHeap)
        if len(self.maxHeap) > len(self.minHeap) + 1:
            self.minHeap.append(heapq._heappop_max(self.maxHeap))
            heapq.heapify(self.minHeap)
        elif len(self.minHeap) > len(self.maxHeap) + 1:
            self.maxHeap.append(heapq.heappop(self.minHeap))
            heapq._heapify_max(self.maxHeap)
        # print(self.maxHeap)
        # print(self.minHeap)
        # print("-------------------------------------------")
    
    def getMedian(self):
        maxHeapLength = len(self.maxHeap)
        minHeapLength = len(self.minHeap)
        if  maxHeapLength == minHeapLength:
            print((self.maxHeap[0] + self.minHeap[0]) / 2)
        elif maxHeapLength > minHeapLength:
            print(self.maxHeap[0])
        else:
            print(self.minHeap[0])
    
findMedian = FindMedian()
findMedian.insertItem(5)
findMedian.getMedian()
findMedian.insertItem(15)
findMedian.getMedian()
findMedian.insertItem(10)
findMedian.getMedian()
findMedian.insertItem(20)
findMedian.getMedian()
findMedian.insertItem(3)
findMedian.getMedian()

# findMedian = FindMedian()
# findMedian.insertItem(5)
# findMedian.getMedian()
# findMedian.insertItem(10)
# findMedian.getMedian()
# findMedian.insertItem(15)
# findMedian.getMedian()

# findMedian = FindMedian()
# findMedian.insertItem(1)
# findMedian.getMedian()
# findMedian.insertItem(2)
# findMedian.getMedian()
# findMedian.insertItem(3)
# findMedian.getMedian()
# findMedian.insertItem(4)
# findMedian.getMedian()

# findMedian = FindMedian()
# findMedian.insertItem(5)
# findMedian.getMedian()
# findMedian.insertItem(2)
# findMedian.getMedian()
# findMedian.insertItem(3)
# findMedian.getMedian()
# findMedian.insertItem(1)
# findMedian.getMedian()
# findMedian.insertItem(6)
# findMedian.getMedian()
# findMedian.insertItem(4)
# findMedian.getMedian()