def deleteIndexedElement(heap, indexToDelete):
    heapLength = len(heap)
    if indexToDelete < heapLength:
        heap[indexToDelete], heap[heapLength - 1] = heap[heapLength - 1], heap[indexToDelete]
        heap = heap[:heapLength - 1]
        newHeapLength = len(heap)
        firstNonLeafNode = (newHeapLength // 2) - 1
        for i in range(firstNonLeafNode, -1, -1):
            heapify(heap, newHeapLength, i)
        print(heap)
    else:
        print("Cannot delete element as the index exceeds the size of heap")

def heapify(heap, n, i):
    leftChildIndex = 2*i + 1
    rightChildIndex = 2*i + 2
    if leftChildIndex < n and heap[leftChildIndex] < heap[i]:
        heap[leftChildIndex], heap[i] = heap[i], heap[leftChildIndex]
        heapify(heap, n, leftChildIndex)
    if rightChildIndex < n and heap[rightChildIndex] < heap[i]:
        heap[rightChildIndex], heap[i] = heap[i], heap[rightChildIndex]
        heapify(heap, n, rightChildIndex)

# [1, 5, 6, 9, 11, 8, 15, 17, 21]
minHeap = [1, 9, 22, 17, 11, 33, 27, 21, 19]
index = 6
deleteIndexedElement(minHeap, index - 1)