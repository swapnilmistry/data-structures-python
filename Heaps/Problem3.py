def getMaxElement(heap):
    heapSize = len(heap)
    maxElement = heap[0]
    firstNonLeafNode = (heapSize // 2) - 1
    for i in range (firstNonLeafNode, -1, -1):
        leftChild = 2*i + 1
        rigthChild = 2*i + 2
        if leftChild < heapSize and heap[2*i + 1] > maxElement:
            maxElement = heap[2*i + 1]
        if rigthChild < heapSize and heap[2*i + 2] > maxElement:
            maxElement = heap[2*i + 2]
    print(maxElement);

# [10, 25, 23, 45, 30, 50]
minHeap = [1, 9, 22, 17, 11, 33, 27, 21, 19]
getMaxElement(minHeap)