def heapify(array, n, i):
    print(array)
    leftChild = 2*i + 1
    rightChild = 2*i + 2
    if (leftChild < n and array[i] < array[leftChild]):
        swap(array, i, leftChild)
        heapify(array, n, leftChild)
    if (rightChild < n and array[i] < array[rightChild]):
        swap(array, i, rightChild)
        heapify(array, n, rightChild)
        
def swap(array, i, childPosition):
    temp = array[i]
    array[i] = array[childPosition]
    array[childPosition] = temp

def heapSort(array):
    firstParentNodePosition = (len(array) // 2) - 1;
    for i in range(firstParentNodePosition, -1, -1):
        heapify(array, len(array), i)
    for i in range(len(array) - 1, 0, -1):
        swap(array, i, 0)
        heapify(array, i, 0)
    

heapSort([10, 20, 15, 12, 40, 25, 18])