# Also called as snake and snail problem | hare and tortorise problem

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

    def insertAtStart(self, data):
        node = Node(data)
        node.next = self.head
        self.head = node

    def insertAtEnd(self, data):
        if self.head == None:
            self.insertAtStart(data)
        else:
            current = self.head
            while current.next != None:
                current = current.next
            node = Node(data)
            current.next = node
            node.next = None
    
    def getLinkedListCount(self):
        if self.head == None:
            return 0
        else:
            current = self.head
            count = 0
            while current != None:
                count = count + 1
                current = current.next
            return count

    # Specify the index where last pointer needs to point.
    # Example if index == 2, the last node of list will point to 2nd node in list creating a circular loop.
    def createLoopInLinkedList(self, index):
        linkedListCount = self.getLinkedListCount()
        if linkedListCount == 0:
            print("Linked list is empty. Cannot create loop.")
            return
        elif index > linkedListCount:
            print("The number excedes length of linked list. Please enter valid number.")
            return
        else:
            current = self.head
            count = 0
            while count < index - 1 :
                count = count + 1
                current = current.next
            temp = current
            while current.next != None:
                current = current.next
            current.next = temp
    
    # Detect if linked list contains loop or not.
    def detectLoop(self):
        if self.head == None:
            print("False")
            return 
        else:
            slowPointer = self.head
            fastPointer = self.head
            while slowPointer.next and fastPointer.next:
                slowPointer = slowPointer.next
                fastPointer = fastPointer.next.next
                if slowPointer == fastPointer:
                    print("Loop Detected.")
                    self.detectLoopLength(slowPointer, fastPointer)
                    slowPointer = self.head # Floyd's algorithm. The distance od loop start will be same from head and current pointer position.
                    while slowPointer:
                        slowPointer = slowPointer.next
                        fastPointer = fastPointer.next
                        if slowPointer == fastPointer:
                            print("The node at which loop has started is:", slowPointer.data)
                            return
            print("No loop detected.")
            return

    def detectLoopLength(self, slowPointer, fastPointer):
        count = 0
        while slowPointer:
            count = count + 1
            slowPointer = slowPointer.next
            if slowPointer == fastPointer:
                print("Length of loop:", count)
                return

    def displayLinkedList(self):
        if self.head == None:
            print("Linked list is empty. No items to display.")
        else:
            current = self.head
            while current != None:
                print(current.data, end = "\n")
                current = current.next

linkedList = LinkedList()
linkedList.insertAtStart(11)
linkedList.insertAtEnd(12)
linkedList.insertAtEnd(13)
linkedList.insertAtEnd(14)
linkedList.insertAtEnd(15)
linkedList.insertAtEnd(16)
linkedList.insertAtEnd(17)
linkedList.createLoopInLinkedList(4)
#linkedList.displayLinkedList()
linkedList.detectLoop()