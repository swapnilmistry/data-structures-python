import math

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

    def insertAtStart(self, data):
        node = Node(data)
        node.next = self.head
        self.head = node
    
    def insertAtEnd(self, data):
        if self.head == None:
            self.insertAtStart(data)
        else:
            current = self.head
            while current.next != None:
                current = current.next
            node = Node(data)
            current.next = node
            node.next = None

    # Approach 1 - Using two pointer. Keep second pointer twice ahead of first. When second pointer reaches end of linked list, the fist pointer will be at middle.
    def FindMiddleNode(self):
        if self.head == None:
            print("Linked list is empty. Cannot find middle node.")
            return
        else:
            slowPointer = self.head
            fastPointer = self.head
            while fastPointer and slowPointer:
                fastPointer = fastPointer.next
                if fastPointer and fastPointer.next:
                    slowPointer = slowPointer.next
                    fastPointer = fastPointer.next
                else:
                    print("Middle node of linked list is:", slowPointer.data)
                    return

    # Approach 2 - Iterative. Get length of linked list and then traverse till n/2 (n = length of linked list) to get middle node of linked list.
    # def FindMiddleNode(self):
    #     current = self.head
    #     count  = 0
    #     while current != None:
    #         count = count + 1
    #         current = current.next
    #     nodePosition = math.ceil(count/2)
    #     current = self.head
    #     count = 0
    #     while count < nodePosition - 1:
    #         count = count + 1
    #         current = current.next
    #     print("Middle node of linked list is:", current.data)

    def displayLinkedList(self):
        if self.head == None:
            print("Linked list is empty. No items to display.")
        else:
            current = self.head
            while current != None:
                print(current.data, end = "\n")
                current = current.next

linkedList = LinkedList()
linkedList.insertAtStart(1)
linkedList.insertAtEnd(2)
linkedList.insertAtEnd(3)
linkedList.insertAtEnd(4)
linkedList.insertAtEnd(5)
linkedList.insertAtEnd(6)
linkedList.insertAtEnd(7)
linkedList.displayLinkedList()
linkedList.FindMiddleNode()