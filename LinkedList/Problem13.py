# Find last element in linked list from the beginning whose n%k = 0.
# n = number of elements in linked list
# k = integer constant

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

    def insertAtStart(self, data):
        node = Node(data)
        node.next = self.head
        self.head = node
    
    def insertAtEnd(self, data):
        if self.head == None:
            self.insertAtStart(data)
        else:
            current = self.head
            while current.next != None:
                current = current.next
            node = Node(data)
            current.next = node
            node.next = None
    
    def modularNodeFromBeginning(self, n):
        if self.head == None:
            return None
        else:
            modularNode = None
            current = self.head
            i = 0
            if n <= 0:
                return None
            while current != None:
                if i % n == 0:
                    modularNode = current
                i = i + 1
                current = current.next
            print(modularNode.data)
    
    # def modularNodeFromEnd(self, n):
    #     if self.head == None:
    #         return None
    #     else:
    #         current = self.head
    #         modularNode = self.head
    #         i = 0
    #         if n <= 0:
    #             return None
    #         while i < n and current != None:
    #             i = i + 1
    #             current = current.next
    #         if current == None:
    #             return
    #         while current != None:
    #             modularNode = modularNode.next
    #             current = current.next
    #         print(modularNode.data)



    def displayLinkedList(self):
        if self.head == None:
            print("Linked list is empty. No items to display.")
        else:
            current = self.head
            while current != None:
                print(current.data, end = "\n")
                current = current.next

linkedList = LinkedList()
linkedList.insertAtStart(1)
linkedList.insertAtEnd(2)
linkedList.insertAtEnd(3)
linkedList.insertAtEnd(4)
linkedList.insertAtEnd(5)
linkedList.insertAtEnd(6)
linkedList.modularNodeFromBeginning(3)
# linkedList.displayLinkedList()