class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

    def insertAtStart(self, data):
        node = Node(data)
        node.next = self.head
        self.head = node
    
    def insertAtEnd(self, data):
        if self.head == None:
            self.insertAtStart(data)
        else:
            current = self.head
            while current.next != None:
                current = current.next
            node = Node(data)
            current.next = node
            node.next = None

    def IsLinkedListEvenOrOdd(self):
        current = self.head
        count  = 0
        while current != None:
            count = count + 1
            current = current.next
        if count % 2 == 0:
            print("Linked list is even.")
        else:
            print("Linked list is odd.")

    def displayLinkedList(self):
        if self.head == None:
            print("Linked list is empty. No items to display.")
        else:
            current = self.head
            while current != None:
                print(current.data, end = "\n")
                current = current.next

linkedList = LinkedList()
linkedList.insertAtStart(1)
linkedList.insertAtEnd(2)
linkedList.insertAtEnd(3)
linkedList.insertAtEnd(4)
linkedList.insertAtEnd(5)
linkedList.insertAtEnd(6)
linkedList.displayLinkedList()
linkedList.IsLinkedListEvenOrOdd()
