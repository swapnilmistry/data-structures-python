# Given a linked list and a value X partion it such that all node less than X comes before nodes greater that or equal to X.
# Input = 2 -> 5 -> 4 -> 3 -> 6 -> 3
# Output = 2 -> 3 -> 3 -> 5 -> 4 -> 6
# where x = 4

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

    def insertAtStart(self, data):
        node = Node(data)
        node.next = self.head
        self.head = node
    
    def insertAtEnd(self, data):
        if self.head == None:
            self.insertAtStart(data)
        else:
            current = self.head
            while current.next != None:
                current = current.next
            node = Node(data)
            current.next = node
            node.next = None
    
    def partitionList(self, x):
        if self.head == None:
            return None
        else:
            lesserValues = []
            greaterValues = []
            current = self.head
            while current != None:
                if current.data < x:
                    lesserValues.append(current.data)
                    current = current.next
                else:
                    greaterValues.append(current.data)
                    current = current.next
            result = lesserValues + greaterValues
            current = self.head
            for item in result:
                current.data = item
                current = current.next


    def displayLinkedList(self):
        if self.head == None:
            print("Linked list is empty. No items to display.")
        else:
            current = self.head
            while current != None:
                print(current.data, end = "\n")
                current = current.next

linkedList = LinkedList()
linkedList.insertAtStart(2)
linkedList.insertAtEnd(5)
linkedList.insertAtEnd(4)
linkedList.insertAtEnd(3)
linkedList.insertAtEnd(6)
linkedList.insertAtEnd(3)
linkedList.partitionList(4)
linkedList.displayLinkedList()