class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

    def insertAtStart(self, data):
        node = Node(data)
        node.next = self.head
        self.head = node
    
    def insertAtEnd(self, data):
        if self.head == None:
            self.insertAtStart(data)
        else:
            current = self.head
            while current.next != None:
                current = current.next
            node = Node(data)
            current.next = node
            node.next = None

    def IsLinkedListPalindrome(self):
        if self.head == None:
            print("Linked list is empty. Cannot determine linked list is palindrome or not.")
            return
        else:
            current = self.head
            string = ""
            while current != None:
                string = string + current.data
                # lst.append(current.data.lower()) To ignore case sensitivity.
                current = current.next
            
            if string == string[::-1]:
                print("Linked list is palindrome.")
            else:
                print("Linked list is not palindrrome.")

    def displayLinkedList(self):
        if self.head == None:
            print("Linked list is empty. No items to display.")
        else:
            current = self.head
            while current != None:
                print(current.data, end = "\n")
                current = current.next

linkedList = LinkedList()
linkedList.insertAtStart("a")
linkedList.insertAtEnd("bc")
linkedList.insertAtEnd("d")
linkedList.insertAtEnd("dcb")
linkedList.insertAtEnd("a")
linkedList.displayLinkedList()
linkedList.IsLinkedListPalindrome()