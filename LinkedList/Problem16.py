# Input : 1 -> 2 -> 3 -> 4 -> 5
# Output : 3 -> 2 -> 1 -> 5 -> 4 where k = 3

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None 

    def insertAtStart(self, data):
        node = Node(data)
        node.next = self.head
        self.head = node
    
    def insertAtEnd(self, data):
        if self.head == None:
            self.insertAtStart(data)
        else:
            current = self.head
            while current.next != None:
                current = current.next
            node = Node(data)
            current.next = node
            node.next = None

    def reverseKNodes(self, head, n):
        current = head
        count = 0
        prevNode = None
        nextNode = None
        while current != None and count < n:
            count = count + 1
            nextNode = current.next
            current.next = prevNode
            prevNode = current
            current = nextNode
        
        if nextNode != None:
            head.next = self.reverseKNodes(nextNode, n)

        return prevNode

    def displayLinkedList(self):
        if self.head == None:
            print("Linked list is empty. No items to display.")
        else:
            current = self.head
            while current != None:
                print(current.data, end = "\n")
                current = current.next

linkedList = LinkedList()
linkedList.insertAtStart(1)
linkedList.insertAtEnd(2)
linkedList.insertAtEnd(3)
linkedList.insertAtEnd(4)
linkedList.insertAtEnd(5)
linkedList.head = linkedList.reverseKNodes(linkedList.head, 3)
linkedList.displayLinkedList()