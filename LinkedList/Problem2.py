# You can also solve this using stack.
# Create separate stack for each linked list.
# Start poping elements of stack storing poped element in temp variable until the elements are different.

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None
        self.head1 = None

    def insertAtStart(self, data):
        node = Node(data)
        node.next = self.head
        self.head = node

    def insertAtEnd(self, data):
        if self.head == None:
            self.insertAtStart(data)
        else:
            current = self.head
            while current.next != None:
                current = current.next
            node = Node(data)
            current.next = node
            node.next = None
    
    def getLinkedListCount(self, head):
        current = head
        count = 0
        while current != None:
            count = count + 1
            current = current.next
        return count

    def createIntersection(self, position):
        linkedListCount = self.getLinkedListCount(self.head)
        if linkedListCount == 0:
            print("Cannot create intersection. Linked list is empty.\n")
            return
        elif position > linkedListCount or position == 1:
            print("Cannot create intersection. The position must be less than length of linked list and greater than 1.\n")
            return
        else:
            node = Node(52) # Create new node with data as 52.
            self.head1 = node
            current = self.head
            count = 0
            while count < position - 1:
                count = count + 1
                current = current.next
            node.next = current

    def detectIntersection(self):
        if self.head == None or self.head1 == None:
            print("\nNo intersection found.")
        else:
            current = self.head
            current1 = self.head1
            lengthOfLinkedList = self.getLinkedListCount(self.head)
            lengthOfLinkedList1 = self.getLinkedListCount(self.head1)
            lengthDifference = abs(lengthOfLinkedList - lengthOfLinkedList1)
            count = 0
            if lengthOfLinkedList > lengthOfLinkedList1:
                while count < lengthDifference:
                    count = count + 1
                    current = current.next
            elif lengthOfLinkedList1 > lengthOfLinkedList:
                while count < lengthDifference:
                    count = count + 1
                    current1 = current1.next
            while current and current1:
                current = current.next
                current1 = current1.next
                if current == current1:
                    print("\n")
                    print("The node at which intersection occurs is:", current.data)
                    return

    def displayLinkedList(self):
        if self.head == None:
            print("Linked list is empty. No items to display.")
        else:
            current = self.head
            current1 = self.head1
            print("Linked list 1: ", end = "\n")
            while current != None:
                print(current.data, end = " ")
                current = current.next
            print("\n")
            print("Linked list 2: ", end = "\n")
            while current1 != None:
                print(current1.data, end = " ")
                current1 = current1.next

linkedList = LinkedList()
linkedList.insertAtStart(11)
linkedList.insertAtEnd(12)
linkedList.insertAtEnd(13)
linkedList.insertAtEnd(14)
linkedList.insertAtEnd(15)
linkedList.insertAtEnd(16)
linkedList.insertAtEnd(17)
linkedList.createIntersection(4)
linkedList.displayLinkedList()
linkedList.detectIntersection()