class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class StackUsingLinkedList:
    def __init__(self):
        self.head = None
    
    def push(self, data):
        node = Node(data)
        if self.head == None:
            self.head = node
        else:
            node.next = self.head
            self.head = node
    
    def pop(self):
        if self.head == None:
            return None
        else:
            popedNode = self.head
            self.head = self.head.next
            popedNode.next = None
            return popedNode.data
    
    def peek(self):
        if self.head == None:
            return None
        else:
            return self.head.data
    
    def display(self):
        if self.head == None:
            print("Stack Underflow.")
        else:
            current = self.head
            while current != None:
                print(current.data, end = " ")
                current = current.next
            
stack = StackUsingLinkedList()
stack.push(1)
stack.push(2)
stack.push(3)
stack.push(4)
stack.push(5)
stack.push(6)
stack.display()
stack.pop()
print("\n")
stack.display()
print("\n")
print(stack.peek())