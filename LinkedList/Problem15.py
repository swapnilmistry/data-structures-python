class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

    def insertAtStart(self, data):
        node = Node(data)
        node.next = self.head
        self.head = node
    
    def insertAtEnd(self, data):
        if self.head == None:
            self.insertAtStart(data)
        else:
            current = self.head
            while current.next != None:
                current = current.next
            node = Node(data)
            current.next = node
            node.next = None
    
    def reorderList(self):
            if self.head == None:
                return
            else:
                newHead = self.findMiddle(self.head)
                newHeadAfterReverse = self.reverseList(newHead)
                p = self.head
                q = newHeadAfterReverse
                while q != None:
                    qnext = q.next
                    q.next = p.next
                    p.next = q
                    p = q.next
                    q = qnext

    def findMiddle(self, head):
        slowPointer = head
        fastPointer = head

        while fastPointer.next and fastPointer.next.next:
            slowPointer = slowPointer.next
            fastPointer = fastPointer.next.next
        head = slowPointer.next
        slowPointer.next = None
        return head

    def reverseList(self, head):
        current = head
        previousNode = None
        nextNode = None

        while current != None:
            nextNode = current.next
            current.next = previousNode
            previousNode = current
            current = nextNode
        
        head = previousNode
        return head

    def displayLinkedList(self):
        if self.head == None:
            print("Linked list is empty. No items to display.")
        else:
            current = self.head
            while current != None:
                print(current.data, end = "\n")
                current = current.next

linkedList = LinkedList()
linkedList.insertAtStart(1)
linkedList.insertAtEnd(2)
linkedList.insertAtEnd(3)
linkedList.insertAtEnd(4)
linkedList.insertAtEnd(5)
linkedList.reorderList()
linkedList.displayLinkedList()