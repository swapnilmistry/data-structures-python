# All even nodes at start and all odd nodes at end.

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

    def insertAtStart(self, data):
        node = Node(data)
        node.next = self.head
        self.head = node
    
    def insertAtEnd(self, data):
        if self.head == None:
            self.insertAtStart(data)
        else:
            current = self.head
            while current.next != None:
                current = current.next
            node = Node(data)
            current.next = node
            node.next = None
    
    def evenAtAtStartOddAtEnd(self):
        if self.head == None:
            return None
        else:
            even = []
            odd = []
            current = self.head
            while current != None:
                if current.data % 2 == 0:
                    even.append(current.data)
                    current = current.next
                else:
                    odd.append(current.data)
                    current = current.next
            result = even + odd
            current = self.head
            for item in result:
                current.data = item
                current = current.next

    def displayLinkedList(self):
        if self.head == None:
            print("Linked list is empty. No items to display.")
        else:
            current = self.head
            while current != None:
                print(current.data, end = "\n")
                current = current.next

linkedList = LinkedList()
linkedList.insertAtStart(1)
linkedList.insertAtEnd(2)
linkedList.insertAtEnd(3)
linkedList.insertAtEnd(4)
linkedList.insertAtEnd(5)
linkedList.insertAtEnd(6)
linkedList.insertAtEnd(7)
linkedList.insertAtEnd(8)
linkedList.evenAtAtStartOddAtEnd()
linkedList.displayLinkedList()