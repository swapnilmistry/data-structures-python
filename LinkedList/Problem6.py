class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

    def insertAtStart(self, data):
        node = Node(data)
        node.next = self.head
        self.head = node
    
    def insertAtEnd(self, data):
        if self.head == None:
            self.insertAtStart(data)
        else:
            current = self.head
            while current.next != None:
                current = current.next
            node = Node(data)
            current.next = node
            node.next = None
    
    # Approach 1
    # def findNthLastNode(self, number):
    #     if self.head == None:
    #         print("Cannot find node. Linked list is empty.")
    #     else:
    #         lengthOfLinkedList = 0
    #         current = self.head
    #         while current != None:
    #             lengthOfLinkedList = lengthOfLinkedList + 1
    #             current = current.next
    #         current = self.head
    #         if number >= lengthOfLinkedList:
    #             print("The number entered is greater than length of linked list.")
    #             return
    #         nodePositionToFind = lengthOfLinkedList - number
    #         count = 0
    #         while count < nodePositionToFind:
    #             count = count + 1
    #             current = current.next
    #         print("The", number, "th last node in linked list is:", current.data)
    
    # Approach 2 - In one scan
    def findNthLastNode(self, number):
        if self.head == None:
            print("Cannot find node. Linked list is empty.")
        else:
            current = self.head
            currentN = self.head
            count = 0
            while count < number - 1:
                count = count + 1
                currentN = currentN.next
            while currentN.next != None:
                current = current.next
                currentN = currentN.next
            print("The", number, "th last node in linked list is:", current.data)

    def displayLinkedList(self):
        if self.head == None:
            print("Linked list is empty. No items to display.")
        else:
            current = self.head
            while current != None:
                print(current.data, end = "\n")
                current = current.next

linkedList = LinkedList()
linkedList.insertAtStart(1)
linkedList.insertAtEnd(2)
linkedList.insertAtEnd(3)
linkedList.insertAtEnd(4)
linkedList.insertAtEnd(5)
linkedList.displayLinkedList()
linkedList.findNthLastNode(4)