class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

    def insertAtStart(self, data):
        node = Node(data)
        node.next = self.head
        self.head = node
    
    def insertAtEnd(self, data):
        if self.head == None:
            self.insertAtStart(data)
        else:
            current = self.head
            while current.next != None:
                current = current.next
            node = Node(data)
            current.next = node
            node.next = None
    
    def sqrtNthNode(self):
        sqrtNode = None
        current = self.head
        i = j = 1
        while current != None:
            if i == j*j:
                if sqrtNode == None:
                    sqrtNode = self.head
                else:
                    sqrtNode = sqrtNode.next
                    j = j + 1
            i + 1
            current = current.next
        print(sqrtNode.data)


    def displayLinkedList(self):
        if self.head == None:
            print("Linked list is empty. No items to display.")
        else:
            current = self.head
            while current != None:
                print(current.data, end = "\n")
                current = current.next

linkedList = LinkedList()
linkedList.insertAtStart(1)
linkedList.insertAtEnd(2)
linkedList.insertAtEnd(3)
linkedList.insertAtEnd(4)
linkedList.insertAtEnd(5)
linkedList.sqrtNthNode()
# linkedList.displayLinkedList()