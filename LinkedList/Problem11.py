class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class CircularLinkedList:
    def __init__(self):
        self.head = None
    
    def push(self, data):
        node = Node(data)
        current = self.head
        node.next = self.head
        if self.head is not None:
            while current.next != self.head:
                current = current.next
            current.next = node
        else:
            node.next = node
        
        self.head = node
    
    def josephusCircle(self, n):
        if self.head == None:
            return None
        else:
            count = 0
            current = self.head
            prev = None
            while current.next != current:
                count = count + 1
                if count == n:
                    count = 0
                    prev.next = current.next
                    current = current.next
                else:
                    prev = current
                    current = current.next
            print(current.data)


    def displayLinkedList(self):
        current = self.head
        if self.head is not None:
            while True:
                print(current.data)
                current = current.next
                if current == self.head:
                    break

circularLinkedList = CircularLinkedList()
circularLinkedList.push(1)
circularLinkedList.push(2)
circularLinkedList.push(3)
circularLinkedList.push(4)
circularLinkedList.push(5)
#circularLinkedList.displayLinkedList()
circularLinkedList.josephusCircle(2)