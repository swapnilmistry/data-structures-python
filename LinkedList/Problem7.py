# Python3 program to find fractional node 
# in a linked list 
import math 

# Linked list node 
class Node: 
	def __init__(self, data): 
		self.data = data 
		self.next = None

# Function to create a new node 
# with given data 
def newNode(data): 
	new_node = Node(data) 
	new_node.data = data 
	new_node.next = None
	return new_node 

# Function to find fractional node 
# in the linked list 
def fractionalNodes(head, k): 
	
	# Corner cases 
	if (k <= 0 or head == None): 
		return None

	fractionalNode = None

	# Traverse the given list 
	i = 0
	temp = head 
	while (temp != None): 
		
		# For every k nodes, we move 
		# fractionalNode one step ahead. 
		if (i % k == 0): 

			# First time we see a multiple of k 
			if (fractionalNode == None): 
				fractionalNode = head 

			else: 
				fractionalNode = fractionalNode.next

		i = i + 1
		temp = temp.next

	return fractionalNode 

# A utility function to print a linked list 
def prList(node): 
	while (node != None): 
		print(node.data, end = ' ') 
		node = node.next

# Driver Code 
if __name__ == '__main__': 
	head = newNode(1) 
	head.next = newNode(2) 
	head.next.next = newNode(3) 
	head.next.next.next = newNode(4) 
	head.next.next.next.next = newNode(5) 
	k = 2

	print("List is", end = ' ') 
	prList(head) 

	answer = fractionalNodes(head, k) 
	print("\nFractional node is", end = ' ') 
	print(answer.data) 
