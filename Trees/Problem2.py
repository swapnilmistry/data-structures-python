class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BinaryTree:

    def preorderTraversalRecurssive(self, root):
        if not root:
            return
        else:
            print(root.data, end=" ")
            self.preorderTraversalRecurssive(root.left)
            self.preorderTraversalRecurssive(root.right)

    def preorderTraversalNonRecurssive(self, root):
        if not root:
            return
        else:
            stack = []
            stack.append(root)
            while stack:
                node = stack.pop()
                print(node.data, end=" ")
                if node.right:
                    stack.append(node.right)
                if node.left:
                    stack.append(node.left)


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.right.right = Node(7)
binaryTree = BinaryTree()
binaryTree.preorderTraversalRecurssive(root)
print("\n")
binaryTree.preorderTraversalNonRecurssive(root)
