from queue import Queue


class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data


class BinaryTree:

    def isTreeIdentical(self, root1, root2):
        if (root1 is None) and (root2 is None):
            return True
        if (root1 is None and root2 is not None) or (root1 is None and root2 is not None):
            return False
        q1 = Queue()
        q2 = Queue()
        q1.put(root1)
        q2.put(root2)
        while (not q1.empty() and not q2.empty()):
            dequeue1 = q1.get()
            dequeue2 = q2.get()
            if dequeue1.data is not dequeue2.data:
                return False
                break
            if dequeue1.left and dequeue2.left:
                q1.put(dequeue1.left)
                q2.put(dequeue2.left)
            elif dequeue1.left or dequeue2.left:
                return False
                break

            if dequeue1.right and dequeue2.right:
                q1.put(dequeue1.right)
                q2.put(dequeue2.right)
            elif dequeue1.right or dequeue2.right:
                return False
                break
        return True


root1 = Node(1)
root1.left = Node(2)
root1.right = Node(3)
root1.left.left = Node(4)
root1.left.right = Node(5)

root2 = Node(1)
root2.left = Node(2)
root2.right = Node(3)
root2.left.left = Node(4)
root1.left.right = Node(5)

binaryTree = BinaryTree()
print("Are binary tree identical iterative : ",
      binaryTree.isTreeIdentical(root1, root2))
