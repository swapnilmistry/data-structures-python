import queue


class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BinaryTree:

    def deepestNodeInBinaryTree(self, root):
        if not root:
            return
        else:
            q = queue.Queue()
            q.put(root)
            while not q.empty():
                dequeue = q.get()
                if dequeue.left:
                    q.put(dequeue.left)
                if dequeue.right:
                    q.put(dequeue.right)
        return dequeue.data


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.right.left = Node(5)
root.right.right = Node(6)
root.right.left.right = Node(7)
root.right.right.right = Node(8)
root.right.left.right.left = Node(9)
binaryTree = BinaryTree()
print("Deepest node in binary tree : ",
      binaryTree.deepestNodeInBinaryTree(root))
