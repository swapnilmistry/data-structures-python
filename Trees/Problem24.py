from queue import Queue;

class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

class BinaryTree:

    def minDepth(self, root):
        if not root:
            return
        q = Queue()
        q.put(root)
        depth = 1
        foundRootNode = False
        while not q.empty():
            count = q.qsize()
            while count > 0:
                dequeue = q.get()
                if not dequeue.left and not dequeue.right:
                    foundRootNode = True
                    break
                else:
                    if dequeue.left:
                        q.put(dequeue.left)
                    if dequeue.right:
                            q.put(dequeue.right)
                count = count - 1
            depth = depth + 1
            if not foundRootNode:
                break
        return depth

root = Node(1) 
root.left = Node(2) 
root.right = Node(3) 
root.left.left = Node(4) 
root.left.right = Node(5)
binaryTree = BinaryTree()
print(binaryTree.minDepth(root))
    

'''

# Python program to find minimum depth of a given Binary Tree 

# A Binary Tree node 
class Node: 
	# Utility to create new node 
	def __init__(self , data): 
		self.data = data 
		self.left = None
		self.right = None

def minDepth(root): 
	# Corner Case 
	if root is None: 
		return 0

	# Create an empty queue for level order traversal 
	q = [] 
	
	# Enqueue root and initialize depth as 1 
	q.append({'node': root , 'depth' : 1}) 

	# Do level order traversal 
	while(len(q)>0): 
		# Remove the front queue item 
		queueItem = q.pop(0) 
	
		# Get details of the removed item 
		node = queueItem['node'] 
		depth = queueItem['depth'] 
		# If this is the first leaf node seen so far 
		# then return its depth as answer 
		if node.left is None and node.right is None:	 
			return depth 
		
		# If left subtree is not None, add it to queue 
		if node.left is not None: 
			q.append({'node' : node.left , 'depth' : depth+1}) 

		# if right subtree is not None, add it to queue 
		if node.right is not None: 
			q.append({'node': node.right , 'depth' : depth+1}) 

# Driver program to test above function 
# Lets construct a binary tree shown in above diagram 
root = Node(1) 
root.left = Node(2) 
root.right = Node(3) 
root.left.left = Node(4) 
root.left.right = Node(5) 
print minDepth(root) 


'''