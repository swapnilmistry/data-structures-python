from queue import Queue


class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BinaryTree:

    def reverseLevelorderTraversal(self, root):
        if root is None:
            return
        else:
            q = Queue()
            stack = []
            q.put(root)
        while not q.empty():
            dequeue = q.get()
            stack.append(dequeue)
            if dequeue.right:
                q.put(dequeue.right)
            if dequeue.left:
                q.put(dequeue.left)
        while stack:
            print(stack.pop().data, end=" ")


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.right.right = Node(7)
binaryTree = BinaryTree()
binaryTree.reverseLevelorderTraversal(root)
