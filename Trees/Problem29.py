class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

class BinarySearchTree:

    def predecessor(self, root):
        if root.left:
            temp = root.left
            while temp and temp.right:
                temp = temp.right
            return temp.data
        else:
            return None
    
    def successor(self, root):
        if root.right:
            temp = root.right
            while temp and temp.left:
                temp = temp.left
            return temp.data
        else:
            return None

root = Node(10)
root.left = Node(6)
root.left.left = Node(4)
root.left.right = Node(9)
root.left.right.left = Node(7)
root.right = Node(16)
root.right.left = Node(13)
binarySearchTree = BinarySearchTree();
print(binarySearchTree.predecessor(root))
print(binarySearchTree.successor(root))