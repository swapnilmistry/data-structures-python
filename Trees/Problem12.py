from queue import Queue

class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

class BinaryTree:
    
    def findTotalLeavesNode(self, root):
        if root is None:
            return
        count = 0
        q = Queue()
        q.put(root)
        while not q.empty():
            dequeue = q.get()
            if dequeue.left:
                q.put(dequeue.left)
            if dequeue.right:
                q.put(dequeue.right)
            if dequeue.left is None  and dequeue.right is None:
                count = count + 1
        print("Total leaves node in binary tree : ", count)
    
    def findTotalLeavesNodeRecurrsive(self, root):
        if root is None:
            return 0
        
        if root.left is None and root.right is None:
            return 1
        else:
            return self.findTotalLeavesNodeRecurrsive(root.left) + self.findTotalLeavesNodeRecurrsive(root.right)

root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.right.right = Node(7)

# root = Node(1) 
# root.left = Node(2) 
# root.right = Node(3) 
# root.left.left = Node(4) 
# root.left.right = Node(5)

binaryTree = BinaryTree()
binaryTree.findTotalLeavesNode(root)
print("Total leaves node in binary tree using recursion : ", binaryTree.findTotalLeavesNodeRecurrsive(root))