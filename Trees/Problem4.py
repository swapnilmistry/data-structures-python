import queue


class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BinaryTree:

    def levelorderTraversalRecurssive(self, root):
        if not root:
            return
        else:
            q = queue.Queue()
            q.put(root)
            while not q.empty():
                dequeue = q.get()
                print(dequeue.data, end=" ")
                if dequeue.left:
                    q.put(dequeue.left)
                if dequeue.right:
                    q.put(dequeue.right)


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.right.right = Node(7)
binaryTree = BinaryTree()
binaryTree.levelorderTraversalRecurssive(root)
