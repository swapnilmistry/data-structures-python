class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

class BinaryTree:

    def __init__(self):
        self.hashTable = dict()

    def verticalSum(self, root, value):
        if root is None:
            return
        # Increment value by 1 if traversing right node
        # Decrement value by 1 if traversing left node
        self.verticalSum(root.left, value-1)
        if value not in self.hashTable:
            self.hashTable[value] = root.data
        else:
             self.hashTable[value] = self.hashTable[value] + root.data
        self.verticalSum(root.right, value+1)

root = Node(1)  
root.left = Node(2)  
root.right = Node(3)  
root.left.left = Node(4)  
root.left.right = Node(5)  
root.right.left = Node(6)  
root.right.right = Node(7) 

binaryTree = BinaryTree()
binaryTree.verticalSum(root, 0)
print(binaryTree.hashTable.values())