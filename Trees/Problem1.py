class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BinaryTree:

    def inorderTraversalRecurssive(self, root):
        if not root:
            return
        else:
            self.inorderTraversalRecurssive(root.left)
            print(root.data, end=" ")
            self.inorderTraversalRecurssive(root.right)

    # def inorderTaversalNonRecurssive(self, root):
    #     if not root:
    #         return
    #     else:
    #         stack = []
    #         node = root
    #         while stack or node:
    #             if node:
    #                 stack.append(node)
    #                 node = node.left
    #             else:
    #                 node = stack.pop()
    #                 print(node.data, end=" ")
    #                 node = node.right

    def inorderTaversalNonRecurssive(self, root):
        if not root:
            return
        else:
            stack = []
            while True:
                if root is not None:
                    stack.append(root)
                    root = root.left
                else:
                    if not stack:
                        break
                    else:
                        root = stack.pop()
                        print(root.data, end=" ")
                        root = root.right


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.right.right = Node(7)
binaryTree = BinaryTree()
binaryTree.inorderTraversalRecurssive(root)
print("\n")
binaryTree.inorderTaversalNonRecurssive(root)
