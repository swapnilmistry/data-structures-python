Problem 1 : Inorder traversal (with and without recussion)

Problem 2 : Preorder traversal (with and without recussion)

Problem 3 : Postorder traversal (with and without recussion)

Problem 4 : Levelorder traversal

Problem 5 : Maximun element in binary tree (with and without recussion)

Problem 6 : Search element in binary tree (with and without recussion)

Problem 7 : Find size of binary tree (Size of binary tree = Total nodes of left subtree + Total nodes of right subtree + 1) 
            Also find max depth (height) of binary tree (i.e -  levels in tree)

Problem 8 : Levelorder traversal in reverse order for a binary tree

Problem 9 : Delete a given binary tree

Problem 10 : Find deepest node in binary tree

Problem 11 : Find diameter of binary tree (Refer https://www.youtube.com/watch?v=ey7DYc9OANo for explanation)

Problem 12 : Find number of leaves nodes (nodes with no children) in binary tree

Problem 13 : Find number of full nodes (nodes with left and right children) in binary tree

Problem 14 : Find number of half nodes (node with atlest one children) in binary tree

Problem 15 : Given a binary tree, delete a node from it by making sure that tree shrinks from the bottom (i.e. the deleted node is replaced by bottom most and rightmost node). This different from BST deletion. 
            Here we do not have any order among elements, so we replace with last element.

            Delete 10 in below tree :

                10
                /    \         
                20     30

            Output : 

                30
                /             
                20     


            Delete 20 in below tree :

                10
                /    \         
                20     30
                        \
                        40
            Output :   

                10
                /   \             
                40    30 

Problem 16 : Insert a node in binary tree (The node will be inserted at after node whose left or right children is None)

Problem 17 : Given a binary tree, find whether thay are identical or not

Problem 18 : Find a level in binary tree which has maximum sum.

Problem 19 : Given a binary tree. Print all root to leaves path

Problem 20 : Print all k-sum paths in a binary tree
            A binary tree and a number k are given. Print every path in the tree with sum of the nodes in the path as k.
            A path can start from any node and end at any node and must be downward only, i.e. they need not be root node and leaf node; and negative numbers can also be there in the tree.
            
            Input : k = 5  
                    Root of below binary tree:
                       1
                    /     \
                  3        -1
                /   \     /   \
               2     1   4     5                        
                    /   / \     \                    
                   1   1   2     6    
                                   
            Output :
            3 2 
            3 1 1 
            1 3 1 
            4 1 
            1 -1 4 1 
            -1 4 2 
            5 
            1 -1 5 

Problem 21 : Convert a binary tree into it's mirror tree

Problem 22 : Given a binary tree, find the maximum path sum. The path may start and end at any node in the tree.

            Input: Root of below tree
           1
          / \
         2   3
    Output: 6

    See below diagram for another example.
    1+2+3

Problem 23 : Write a function to print ZigZag order traversal of a binary tree. For the below binary tree the zigzag order traversal will be 1 3 2 7 6 5 4

                       1
                    /     \
                  2        3
                /   \     /   \
               7     6   5     4                        

Problem 24 : Find minimum depth in binary tree

Problem 25 : Find maxium depth in binary tree

Problem 26 : Given a Binary Tree, find the vertical sum of the nodes that are in the same vertical line. Print all sums through different vertical lines.
              Examples:

                    1
                  /   \
                2      3
              / \    / \
              4   5  6   7
              The tree has 5 vertical lines

              Vertical-Line-1 has only one node 4 => vertical sum is 4
              Vertical-Line-2: has only one node 2=> vertical sum is 2
              Vertical-Line-3: has three nodes: 1,5,6 => vertical sum is 1+5+6 = 12
              Vertical-Line-4: has only one node 3 => vertical sum is 3
              Vertical-Line-5: has only one node 7 => vertical sum is 7

              So expected output is 4, 2, 12, 3 and 7

Problem 27 : Search element in Binary Search Tree

Problem 28 : Find minimum and maximum element in Binary Search Tree

Problem 29: Find Predesessor (maximum value in left subtree) and successor (minimum value in right subtree)

Problem 30 : Insert and Delete and element in Binary Search Tree

Problem 31 : Given pointer to two nodes in Binary Search Tree, find LCA (least common ancestor).  Assume that both variable already exists. (a <= root.data <= b)
             (Note - Give an algorithm for finding sortest path between two nodes in Binary Search Tree is nothing but finding LCA og two nodes in Binary Search Tree)

Problem 32 : Check whether the given tree is binary or not