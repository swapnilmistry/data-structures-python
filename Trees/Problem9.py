class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BinaryTree:
    # Use postorder traversal to delete binary tree, since child nodes needs to be deleted before parent
    def deleteBinaryTree(self, root):
        if root is None:
            return
        else:
            self.deleteBinaryTree(root.left)
            self.deleteBinaryTree(root.right)
            print("Deleting Node : ", root.data)
            del root


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
binaryTree = BinaryTree()
binaryTree.deleteBinaryTree(root)