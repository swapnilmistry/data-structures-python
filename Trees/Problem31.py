class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

class BinarySearchTree:

    def leastCommonAncestor(self, root, a, b):
        if root is None:
            return None
        if b <= root.data <= a or a <= root.data <= b:
            return root.data
        if a > root.data and b > root.data:
            return self.leastCommonAncestor(root.right, a, b)
        if a < root.data and b < root.data:
            return self.leastCommonAncestor(root.left, a, b)

root = Node(20) 
root.left = Node(8) 
root.right = Node(22) 
root.left.left = Node(4) 
root.left.right = Node(12) 
root.left.right.left = Node(10) 
root.left.right.right = Node(14) 

binarySearchTree = BinarySearchTree()
print(binarySearchTree.leastCommonAncestor(root, 10, 14))
print(binarySearchTree.leastCommonAncestor(root, 14, 8))
print(binarySearchTree.leastCommonAncestor(root, 10, 22))