class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BinaryTree:

    def postorderTraversalRecurssive(self, root):
        if not root:
            return
        else:
            self.postorderTraversalRecurssive(root.left)
            self.postorderTraversalRecurssive(root.right)
            print(root.data, end=" ")

    def postorderTraversalNonRecurssive(self, root):
        if not root:
            return
        else:
            stack1 = []
            stack2 = []
            stack1.append(root)
            while stack1:
                root = stack1.pop()
                stack2.append(root)
                if root.left:
                    stack1.append(root.left)
                if root.right:
                    stack1.append(root.right)
            while stack2:
                print(stack2.pop().data, end=" ")


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.right.right = Node(7)
binaryTree = BinaryTree()
binaryTree.postorderTraversalRecurssive(root)
print("\n")
binaryTree.postorderTraversalNonRecurssive(root)
