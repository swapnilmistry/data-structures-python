class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

class BinarySearchTree:
    
    def searchElement(self, root, elementToSearch):
        currentNode = root
        while currentNode:
            if currentNode.data == elementToSearch:
                return currentNode
            elif elementToSearch < currentNode.data:
                currentNode = currentNode.left
            else:
                currentNode = currentNode.right
        return None

root = Node(10)
root.left = Node(6)
root.left.left = Node(4)
root.left.right = Node(9)
root.left.right.left = Node(7)
root.right = Node(16)
root.right.left = Node(13)
binarySearchTree = BinarySearchTree();
print(binarySearchTree.searchElement(root, 7))