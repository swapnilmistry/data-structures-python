from decimal import Decimal


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None
    
class BinaryTree:
    def __init__(self):
        self.maximumPathSum = Decimal("-Infinity")
    def maxPathSum(self, root):
      self.postOrderTraversal(root)
      print(self.maximumPathSum)
    def postOrderTraversal(self, root):
        if root == None:
            return 0
        leftTree = max(self.postOrderTraversal(root.left), 0)
        rightTree = max(self.postOrderTraversal(root.right), 0)
        self.maximumPathSum = max(self.maximumPathSum, leftTree + rightTree + root.data)
        return max(leftTree, rightTree) + root.data


root = Node(10) 
root.left = Node(2) 
root.right = Node(10); 
root.left.left = Node(20); 
root.left.right = Node(1); 
root.right.right = Node(-25); 
root.right.right.left = Node(3); 
root.right.right.right = Node(4);
binaryTree = BinaryTree()
binaryTree.maxPathSum(root)