class Node:
    def __init__(self, data):
        self.data = data;
        self.left = None
        self.right = None

class BinarySearchTree:
    def __init__(self):
        self.isTreeBinary = True
        self.previousValue = 0;

    def isBinaryTree(self, root):
        if root is None:
            return True
        self.isBinaryTree(root.left)
        if self.previousValue > root.data:
            self.isTreeBinary = False
        self.previousValue = root.data
        self.isBinaryTree(root.right)

root = Node(20) 
root.left = Node(8) 
root.right = Node(22) 
root.left.left = Node(4) 
root.left.right = Node(12) 
root.left.right.left = Node(10) 
root.left.right.right = Node(14)
binarySearchTree = BinarySearchTree()
binarySearchTree.isBinaryTree(root)
print(binarySearchTree.isTreeBinary)