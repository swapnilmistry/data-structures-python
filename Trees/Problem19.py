class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

class BinaryTree:

# Print all paths in binary tree

    # def getAllPaths(self, root, path):
    #     if root is None:
    #         return
    #     elif (not len(path)) :
    #         path = path + [root.data]
    #     print(path, end="\n")
    #     if root.left:
    #         self.getAllPaths(root.left, path + [root.left.data])
    #     if root.right:
    #         self.getAllPaths(root.right, path + [root.right.data])

# Print all paths from root to leaves

    def getAllPaths(self, root, path):
        if root is None:
            return
        path = path + [root.data]
        if root.left:
            self.getAllPaths(root.left, path)
        if root.right:
            self.getAllPaths(root.right, path)
        if root.left is None and root.right is None:
            print(path)

root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.right.right = Node(7)

binaryTree = BinaryTree()

# Driver for print all paths in binary tree
# binaryTree.getAllPaths(root, [])

# Driver for print all paths from root to leaves
binaryTree.getAllPaths(root, [])