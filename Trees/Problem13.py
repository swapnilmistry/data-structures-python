from queue import Queue


class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data


class BinaryTree:

    def findTotalFullNode(self, root):
        if root is None:
            return
        count = 0
        q = Queue()
        q.put(root)
        while not q.empty():
            dequeue = q.get()
            if dequeue.left:
                q.put(dequeue.left)
            if dequeue.right:
                q.put(dequeue.right)
            if dequeue.left is not None and dequeue.right is not None:
                count = count + 1
        print("Total full node in binary tree : ", count)


root = Node(2)
root.left = Node(7)
root.right = Node(5)
root.left.right = Node(6)
root.left.right.left = Node(1)
root.left.right.right = Node(11)
root.right.right = Node(9)
root.right.right.left = Node(4)

# root = Node(1)
# root.left = Node(2)
# root.right = Node(3)
# root.left.left = Node(4)
# root.left.right = Node(5)

binaryTree = BinaryTree()
binaryTree.findTotalFullNode(root)
