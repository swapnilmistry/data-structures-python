class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

class BinarySearchTree:

    def findMinElement(self, root):
        currentNode = root
        if currentNode is None:
            return None
        while currentNode and currentNode.left:
            currentNode = currentNode.left
        return currentNode.data
    
    def findMaxElement(self, root):
        currentNode = root
        if currentNode is None:
            return None
        while currentNode and currentNode.right:
            currentNode = currentNode.right
        return currentNode.data
    
    def findMinElementRecusrrsive(self, root):
        if root.left is None:
            return root.data
        return self.findMinElementRecusrrsive(root.left)
    
    def findMaxElementRecusrrsive(self, root):
        if root.right is None:
            return root.data
        return self.findMaxElementRecusrrsive(root.right)
        

root = Node(10)
root.left = Node(6)
root.left.left = Node(4)
root.left.right = Node(9)
root.left.right.left = Node(7)
root.right = Node(16)
root.right.left = Node(13)
binarySearchTree = BinarySearchTree();
print(binarySearchTree.findMinElement(root))
print(binarySearchTree.findMaxElement(root))
# print(binarySearchTree.findMinElementRecusrrsive(root))
# print(binarySearchTree.findMaxElementRecusrrsive(root))
