from queue import Queue;

class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

class BinaryTree:

    def printUtil(self, items):
        for item in items:
            print(item, end=" ");

    def zigZagTraversal(self, root):
        if not root:
            return
        q = Queue()
        q.put(root)
        currentLevel = 1
        currentLevelItems = []
        print(root.data, end=" ");
        while not q.empty():
            count = q.qsize()
            while count > 0:
                dequeue = q.get()
                if dequeue.left:
                    q.put(dequeue.left)
                    currentLevelItems.append(dequeue.left.data)
                if dequeue.right:
                    q.put(dequeue.right)
                    currentLevelItems.append(dequeue.right.data)
                count = count - 1
            if currentLevel % 2 == 0:
                self.printUtil(currentLevelItems)
            else:
                self.printUtil(currentLevelItems[::-1])
            currentLevelItems = []
            currentLevel = currentLevel + 1

root = Node(1) 
root.left = Node(2) 
root.right = Node(3) 
root.left.left = Node(7) 
root.left.right = Node(6) 
root.right.left = Node(5) 
root.right.right = Node(4)
binaryTree = BinaryTree()
binaryTree.zigZagTraversal(root)
        