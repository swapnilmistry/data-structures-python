from queue import Queue


class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data


class BinaryTree:

    def preOrderTraversal(self, root):
        if root is None:
            return
        print(root.data, end=" ")
        self.preOrderTraversal(root.left)
        self.preOrderTraversal(root.right)

    def deleteNodeInBinaryTree(self, root, key):
        if root is None:
            return
        if root.left is None and root.right is None:
            if root.data == nodeToDelete:
                return None
            return root
        q = Queue()
        q.put(root)
        nodeTodelete = None
        deepestNode = None
        while not q.empty():
            dequeue = q.get()
            deepestNode = dequeue
            if dequeue.data == key:
                nodeTodelete = dequeue
            if dequeue.left:
                q.put(dequeue.left)
            if dequeue.right:
                q.put(dequeue.right)
        if nodeTodelete:
            self.deleteDeepestNode(root, deepestNode)
            nodeTodelete.data = deepestNode.data
        return root

    def deleteDeepestNode(self, root, deepestNode):
        q = Queue()
        q.put(root)
        while not q.empty():
            dequeue = q.get()
            if dequeue == deepestNode:
                dequeue = None
                return
            if dequeue.left:
                if dequeue.left == deepestNode:
                    dequeue.left = None
                    return
                q.put(dequeue.left)
            if dequeue.right:
                if dequeue.right == deepestNode:
                    dequeue.right = None
                    return
                q.put(dequeue.right)

root = Node(10)
root.left = Node(11)
root.left.left = Node(7)
root.left.right = Node(12)
root.right = Node(9)
root.right.left = Node(15)
root.right.right = Node(8)
binaryTree = BinaryTree()
print("The tree before the deletion : ", end=" ")
binaryTree.preOrderTraversal(root)
key = 11
root = binaryTree.deleteNodeInBinaryTree(root, key)
print("\n")
print("The tree after the deletion : ", end=" ")
binaryTree.preOrderTraversal(root)
