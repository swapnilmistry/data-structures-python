from queue import Queue


class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BinaryTree:

    def preorderTraversalRecurssive(self, root):
        if not root:
            return
        else:
            print(root.data, end=" ")
            self.preorderTraversalRecurssive(root.left)
            self.preorderTraversalRecurssive(root.right)

    def insertNodeInBinaryTree(self, root, valueToInsert):
        if root is None:
            return
        else:
            q = Queue()
            q.put(root)
        while not q.empty():
            dequeue = q.get()
            if dequeue.left is None:
                dequeue.left = Node(valueToInsert)
                break
            if dequeue.right is None:
                dequeue.right = Node(valueToInsert)
                break
            if dequeue.left:
                q.put(dequeue.left)
            if dequeue.right:
                q.put(dequeue.right)
        return root


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.right.right = Node(7)
binaryTree = BinaryTree()
valueToInsert = 123
binaryTree.preorderTraversalRecurssive(root)
root = binaryTree.insertNodeInBinaryTree(root, valueToInsert)
print("\n")
binaryTree.preorderTraversalRecurssive(root)
