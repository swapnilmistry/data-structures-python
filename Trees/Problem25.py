from queue import Queue;

class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

class BinaryTree:

    def maxDepth(self, root):
        if not root:
            return
        q = Queue()
        q.put(root)
        depth = 0
        while not q.empty():
            count = q.qsize()
            while count > 0:
                dequeue = q.get()
                if dequeue.left:
                    q.put(dequeue.left)
                if dequeue.right:
                        q.put(dequeue.right)
                count = count - 1
            depth = depth + 1
        return depth

root = Node(1) 
root.left = Node(2) 
root.right = Node(3) 
root.left.left = Node(4) 
root.left.right = Node(5) 
binaryTree = BinaryTree()
print(binaryTree.maxDepth(root))
    
