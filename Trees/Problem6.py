from decimal import Decimal
from queue import Queue


class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BinaryTree:

    def __init__(self):
        self.elementFound = False
        self.elementFoundNonRecurssive = False

    # Using pre order travarsal for recurssive approach
    def maxElementInBinaryTreeRecussive(self, root, elementToSearch):
        if root is None:
            return
        else:
            if root.data == elementToSearch:
                self.elementFound = True
                return
            self.maxElementInBinaryTreeRecussive(root.left, elementToSearch)
            self.maxElementInBinaryTreeRecussive(root.right, elementToSearch)

    # Using level order traversal for non recurssive approach
    def maxElementInBinaryTreeNonRecussive(self, root, elementToSearch):
        if root is None:
            return
        else:
            q = Queue()
            q.put(root)
            while not q.empty():
                dequeue = q.get()
                if dequeue.data == elementToSearch:
                    self.elementFoundNonRecurssive = True
                    return
                if dequeue.left:
                    q.put(dequeue.left)
                if dequeue.right:
                    q.put(dequeue.right)


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(100)
root.right.left = Node(6)
root.right.right = Node(7)
binaryTree = BinaryTree()
binaryTree.maxElementInBinaryTreeRecussive(root, 12)
print("Element present in tree using recurssive approach : ",
      binaryTree.elementFound)
print("\n")
binaryTree.maxElementInBinaryTreeNonRecussive(root, 1)
print("Element present in tree using non recurssive approach : ",
      binaryTree.elementFoundNonRecurssive)
