from queue import Queue


class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data


class BinaryTree:

    def levelWithMaxSum(self, root):
        if root is None:
            return
        q = Queue()
        q.put(root)
        maxSum = root.data
        levelWithMaxSum = 0
        currentLevel = 0
        while True:
            count = q.qsize()
            currentLevelSum = 0
            if count == 0:
                break
            currentLevel = currentLevel + 1
            while count > 0:
                dequeue = q.get()
                if dequeue.left:
                    q.put(dequeue.left)
                    currentLevelSum = currentLevelSum + dequeue.left.data
                if dequeue.right:
                    q.put(dequeue.right)
                    currentLevelSum = currentLevelSum + dequeue.right.data
                count = count - 1
            if currentLevelSum > maxSum:
                maxSum = currentLevelSum
                levelWithMaxSum = currentLevel
        return [maxSum, levelWithMaxSum]

# root = Node(1)
# root.left = Node(2)
# root.right = Node(3)
# root.left.left = Node(4)
# root.left.right = Node(5)
# root.right.left = Node(6)
# root.right.right = Node(7)


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.right = Node(8)
root.right.right.left = Node(6)
root.right.right.right = Node(7)

binaryTree = BinaryTree()
maxSum, levelWithMaxSum = binaryTree.levelWithMaxSum(root)
print("Level with max sum : ", levelWithMaxSum)
print("Sum of the level printed above : ", maxSum)
