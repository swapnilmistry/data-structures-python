from queue import Queue


class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BinaryTree:

    # Recurssivelly calculate left and right sub tree length.
    def findSizeofBinaryTreeRecurssive(self, root):
        sizeLeft = 0
        sizeRight = 0
        if not root:
            return
        if root.left:
            sizeLeft = self.findSizeofBinaryTreeRecurssive(root.left)
        if root.right:
            sizeRight = self.findSizeofBinaryTreeRecurssive(root.right)
        return sizeLeft + sizeRight + 1

    # Recurssivelly calculate left and right sub tree length. Add one to largest subtree and return
    def findHeightofBinaryTreeRecurssive(self, root):
        sizeLeft = 0
        sizeRight = 0
        if not root:
            return
        if root.left:
            sizeLeft = self.findHeightofBinaryTreeRecurssive(root.left)
        if root.right:
            sizeRight = self.findHeightofBinaryTreeRecurssive(root.right)

        if sizeLeft > sizeRight:
            return sizeLeft + 1
        else:
            return sizeRight + 1

    # Using level order traversal for non recurssive approach
    def findSizeofBinaryTree(self, root):
        if not root:
            return 0
        else:
            count = 1
            q = Queue()
            q.put(root)
            while not q.empty():
                dequeue = q.get()
                if dequeue.left:
                    q.put(dequeue.left)
                    count = count + 1
                if dequeue.right:
                    q.put(dequeue.right)
                    count = count + 1
            return count

    # Using level order traversal for non recurssive approach and  increment the count once each level is traversed
    def findHeightofBinaryTree(self, root):
        if not root:
            return 0
        else:
            q = Queue()
            height = 0
            q.put(root)
            while True:
                nodeCount = q.qsize()
                if nodeCount == 0:
                    return height
                height = height + 1
                while nodeCount > 0:
                    dequeue = q.get()
                    if dequeue.left:
                        q.put(dequeue.left)
                    if dequeue.right:
                        q.put(dequeue.right)
                    nodeCount = nodeCount - 1


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.right.right = Node(7)
binaryTree = BinaryTree()
print("Size of binary tree using recurssive approach : ",
      binaryTree.findSizeofBinaryTreeRecurssive(root))
print("Size of binary tree using non recurssive approach : ",
      binaryTree.findSizeofBinaryTree(root))
print("Height of binary tree using recurssive approach : ",
      binaryTree.findHeightofBinaryTreeRecurssive(root))
print("Height of binary tree using non recurssive approach : ",
      binaryTree.findHeightofBinaryTree(root))
