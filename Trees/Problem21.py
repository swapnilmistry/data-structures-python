class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BinaryTree:

    def inorderTraversalRecurssive(self, root):
        if not root:
            return
        else:
            self.inorderTraversalRecurssive(root.left)
            print(root.data, end=" ")
            self.inorderTraversalRecurssive(root.right)
    
    def mirrorTree(self, root):
        if not root:
            return
        self.mirrorTree(root.left)
        self.mirrorTree(root.right)
        temp = root.left
        root.left = root.right
        root.right = temp
        return root


root = Node(1)  
root.left = Node(2)  
root.right = Node(3)  
root.left.left = Node(4)  
root.left.right = Node(5)
binaryTree = BinaryTree()
print("Binary tree before mirror : ")
binaryTree.inorderTraversalRecurssive(root)
print("\n")
print("Binary tree after mirror : ")
mirror =  binaryTree.mirrorTree(root);
binaryTree.inorderTraversalRecurssive(mirror)
