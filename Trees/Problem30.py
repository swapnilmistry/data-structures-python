class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

class BinarySearchTree:

    def insertElement(self, root, elementToInsert):
        if root is None:
            root = Node(elementToInsert)
            return root
        else:
            while root:
                if elementToInsert < root.data:
                    if root.left is None:
                        root.left = Node(elementToInsert)
                        return
                    else:
                        root = root.left
                elif elementToInsert > root.data:
                    if root.right is None:
                        root.right = Node(elementToInsert)
                        return
                    else:
                        root = root.right
                else:
                    print("Cannot insert " +str(elementToInsert)+ " as element already exists in tree.")
                    return

    def deleteElement(self, root, elementToDelete):
        # Code to delete element   
    
    def preorderTraversalRecurssive(self, root):
        if not root:
            return
        else:
            print(root.data, end=" ")
            self.preorderTraversalRecurssive(root.left)
            self.preorderTraversalRecurssive(root.right)

binarySearchTree = BinarySearchTree()
root = None;
root = binarySearchTree.insertElement(root, 10)
binarySearchTree.insertElement(root, 6)
binarySearchTree.insertElement(root, 16)
binarySearchTree.insertElement(root, 4)
binarySearchTree.insertElement(root, 9)
binarySearchTree.insertElement(root, 13)
binarySearchTree.insertElement(root, 7)
binarySearchTree.preorderTraversalRecurssive(root)