from decimal import Decimal
from queue import Queue


class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BinaryTree:

    def __init__(self):
        self.maxElement = Decimal("-Infinity")
        self.maxElementNonRecurssive = Decimal("-Infinity")

    # Using pre order travarsal for recurssive approach
    def maxElementInBinaryTreeRecussive(self, root):
        if root is None:
            return
        else:
            if root.data > self.maxElement:
                self.maxElement = root.data
            self.maxElementInBinaryTreeRecussive(root.left)
            self.maxElementInBinaryTreeRecussive(root.right)

    # Using level order traversal for non recurssive approach
    def maxElementInBinaryTreeNonRecussive(self, root):
        if root is None:
            return
        else:
            q = Queue()
            q.put(root)
            while not q.empty():
                dequeue = q.get()
                if dequeue.data > self.maxElementNonRecurssive:
                    self.maxElementNonRecurssive = dequeue.data
                if dequeue.left:
                    q.put(dequeue.left)
                if dequeue.right:
                    q.put(dequeue.right)


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(100)
root.right.left = Node(6)
root.right.right = Node(7)
binaryTree = BinaryTree()
binaryTree.maxElementInBinaryTreeRecussive(root)
print("Max element using recurssive approach : ", binaryTree.maxElement)
print("\n")
binaryTree.maxElementInBinaryTreeNonRecussive(root)
print("Max element using non recurssive approach : ",
      binaryTree.maxElementNonRecurssive)
