'''
1: Iterate from arr[1] to arr[n] over the array. 
2: Compare the current element (key) to its predecessor. 
3: If the key element is smaller than its predecessor, compare it to the elements before. Move the greater elements one position up to make space for the swapped element

'''
def insertionSort(array):
    lengthOfArray = len(array)
    for i in range(1, lengthOfArray):
        hole = i
        while hole > 0 and array[hole - 1] > array[hole]:
            array[hole], array[hole - 1] = array[hole -  1], array[hole]
            hole = hole - 1
    print(array)

insertionSort([12, 11, 13, 5, 6])