'''
array = [64, 25, 12, 22, 11]

1. Find the largest number in array [0...4]
2. Swap the same with last element of array
=> 11 25 12 22 64

1. Find the largest number in array [0...3]
2. Swap the same with last element of array
=> 11 22 12 25 65

1. Find the largest number in array [0...2]
2. Swap the same with last element of array
=> 11 12 22 25 65
'''

def selectionSortByLargest(array):
    lengthOfArray = len(array)
    largestElementIndex = 0
    for i in range(lengthOfArray):
        for j in range(lengthOfArray - i):
            if array[j] > array[largestElementIndex]:
                largestElementIndex = j
        array[largestElementIndex], array[lengthOfArray - i - 1] = array[lengthOfArray - i - 1], array[largestElementIndex]
        largestElementIndex = 0
    print(array)


'''
array = [64, 25, 12, 22, 11]

1. Find the smallest number in array [0...4]
2. Swap the same with last element of array
=> 11 25 12 22 64

1. Find the largest number in array [1...4]
2. Swap the same with last element of array
=> 11 12 25 22 64

1. Find the largest number in array [2...4]
2. Swap the same with last element of array
=> 11 12 22 25 65
'''
def selectionSortBySmallest(array):
    lengthOfArray = len(array)
    for i in range(lengthOfArray):
        smallestElementIndex = i
        for j in range(i, lengthOfArray):
            if array[j] < array[smallestElementIndex]:
                smallestElementIndex = j
        array[smallestElementIndex], array[i] = array[i], array[smallestElementIndex]
    print(array)


# selectionSortByLargest([64, 25, 12, 22, 11])
selectionSortBySmallest([64, 25, 12, 22, 11])
