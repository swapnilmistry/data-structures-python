def HanoiTower(numberOfPlates, t1, t2, tInternetidate):
    if(numberOfPlates > 0):
        HanoiTower(numberOfPlates - 1, t1, tInternetidate, t2)
        print("Move disk from ", t1, "to", t2)
        HanoiTower(numberOfPlates - 1, tInternetidate, t2, t1)

HanoiTower(5, 'A', 'C', 'B')
