def checkIfArrayIsSorted(arr):
    if len(arr) == 0 or len(arr) == 1:
        return True
    return arr[0] < arr[1] and checkIfArrayIsSorted(arr[1:])

if checkIfArrayIsSorted([1,12,3,4,5,6,7]):
    print("True")
else:
    print("False")