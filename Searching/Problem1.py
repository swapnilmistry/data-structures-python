'''
1. Iterate over array and keep a map ot each item having index and count (number of occurance)
2. Increment the count of item if encountered again in array
3. Iterate over map and print the item having index less that length of array

'''

# Method 1 - Without using inbuilt functions
def firstRepeatingNumber(array):
    numbersMap = {}
    isElementRepeated = False
    for i in range(len(array)):
        if array[i] not in numbersMap:
            numbersMap[array[i]] = {
                "index": i,
                "count": 1
            }
        else:
            isElementRepeated = True
            currentElement = numbersMap[array[i]]
            currentElement["count"] = currentElement["count"] + 1
    if isElementRepeated:
        maxIndex = len(array) - 1
        for item in numbersMap.values():
            if item["index"] < maxIndex and item["count"] > 1:
                maxIndex = item["index"]
        print("The first repeated element in array is: ", array[maxIndex])
    else:
        print("No repeated elements in array")

firstRepeatingNumber([6, 10, 5, 4, 9, 120, 4, 6, 10])
# firstRepeatingNumber([10, 5, 3, 4, 3, 5, 6])
# firstRepeatingNumber([1, 2, 3, 4, 5])

# Method 2 - Using inbuilt function

# def firstRepeatingNumber(array):
#     for i in range(len(array)):
#         if array.count(array[i]) > 1:
#             return array[i]
#         return "No repeated elements in array"
# print(firstRepeatingNumber([1, 2, 3, 4]))